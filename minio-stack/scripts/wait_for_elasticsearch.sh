#!/bin/bash

until [[ $(curl -Lksm1 $ES -o/dev/null -w '%{http_code}') == "200" ]]
do
  printf '.'
  sleep ${RANDOM:0:1}
done && echo "ElasticSearch is UP"
