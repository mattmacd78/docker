#!/bin/sh
unalias rm facter curl jq 2>/dev/null
alias ll='ls -l'
URL=elasticsearch:9200
PS1="# es@$URL > "
curl() { /usr/bin/curl -w '\n' --buffer -sH'Content-Type: application/json' ${@} | jq . -crM ; }
mktemp() { TMP=$(/usr/bin/mktemp) ; }
rmtemp() { jq -crM . $TMP ; rm -fr $TMP ; }
GET()    { curl -XGET    ${URL}/${@} ; }
PUT()    { curl -XPUT    ${URL}/${@} ; }
POST()   { curl -XPOST   ${URL}/${@} ; }
DELETE() { curl -XDELETE ${URL}/${@} ; }
_template() {
  mktemp ; cat > $TMP << EOF
{"index_patterns":["test*"],"settings":{"number_of_shards":1,"number_of_replicas":0,"auto_expand_replicas":"0-all","refresh_interval":"-1","index.write.wait_for_active_shards":1,"index.requests.cache.enable":"true","index.queries.cache.enabled":"true","translog":{"sync_interval":"100ms","durability":"async"}},"aliases":{"test":{}}}
EOF
  rmtemp
}
es_bulk() {
    sleep ${RANDOM:0:1}
    start_time=$(date "+%Y.%m.%d.%H.%M.%S")
    PUT _template/test -d $(_template)
    time for ZIP in $( ls -1 /opt/data/???.bulk.json.zip )
      do
        unzip $ZIP -d .tmp/ &>/dev/null
        JSON=$(basename $ZIP .zip)
        split -l 40000 .tmp/$JSON .tmp/$JSON.
        rm .tmp/$JSON
        for FILE in $(ls -1 .tmp/$JSON.*|grep -v .zip$)
        do
          POST test-$start_time/_doc/_bulk --data-binary @$FILE
          rm $FILE
        done
    done 1> .tmp/bulk.$start_time.stdout 2> .tmp/bulk.$start_time.stderr
    POST test-$start_time/_freeze
    rm -fr bulk.$start_time.std*
}
es_cleanup() {
  rm -v .tmp/*{.json.,.std}*
}
es_bulk_errors() {
    while true
    do
        jq -crM ".items[].index|select(.error != null)|.error" .tmp/*stdout
        sleep 0.333
    done
}
es_bulk_monitor() {
    tail -f .tmp/bulk.*.stdout | jq --unbuffered -M .took/1000 2>/dev/null
}
es_search() {
    #sleep ${RANDOM:0:3}
    while true
    do
        GET test/_search?size=10000 | jq 2>/dev/null --unbuffered -crM " . | [ .took/1000 , .hits.total ] "
        sleep 0.333
    done
}

_cat_tasks() {
    GET _cat/tasks?format=json | jq -cr .
}

es_refresh() {
    GET test/_refresh | jq -cr .
    GET test/_search?size=0 | jq -cr .
}

es_tasks_watch() {
    while true
    do
        _cat_tasks | jq -cr " [ .[] | [ .action , .running_time , ( now-( .start_time | tonumber/1000 ) ) ] ] | sort_by( [1] ) | .[] " | grep -v 'monitor/tasks/lists | jq -cr . '
        sleep 0.333
        echo
    done
}
